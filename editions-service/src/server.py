from flask import Flask, request
from flask_restful import Resource, Api
import json 

app = Flask(__name__)
api = Api(app)


class Editions(Resource):
    def get(self):        
        return {
                    'records': [
                                    { 'id': 'itd1', 'name': ' IT Days 2013'},
                                    { 'id': 'itd2', 'name': ' IT Days 2014'},
                                    { 'id': 'itd3', 'name': ' IT Days 2015'},
                                    { 'id': 'itd4', 'name': ' IT Days 2016'},
                                    { 'id': 'itd5', 'name': ' IT Days 2017'},
                                    { 'id': 'itd6', 'name': ' IT Days 2018'}                
                                ]
                } 

api.add_resource(Editions, '/api/editions')

if __name__ == "__main__":
    app.run(debug=False, port= 3003, host='0.0.0.0')
    #app.run(debug=False, port: 3003)


