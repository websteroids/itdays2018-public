﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace authors_service.Controllers
{

    public class Author {
            public int id;

            public string name;
                
            public Author(int _id, string _name) {
                this.id = _id;
                this.name = _name;
            }

    }

    [Route("api/authors")]
    [ApiController]
    public class AuthorsController : ControllerBase
    {
        private List<Author> authors = new List<Author>();

        public AuthorsController() {
            authors.Add(new Author(1, "Radu Vunvulea"));
            authors.Add(new Author(2, "Tudor Damian"));
            authors.Add(new Author(3, "Ciprian Sorlea"));
        }
        
        [HttpGet]
        public ActionResult<IList<Author>> Get()
        {
            return this.authors;
        }

       
    }
}
