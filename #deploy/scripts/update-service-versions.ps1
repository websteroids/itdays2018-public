(Get-Content authentication-service.yaml).replace('auth-service:latest', 'auth-service:' + $Env:RELEASE_ARTIFACTS_AUTH_SERVICE_BUILDID) | Set-Content authentication-service.yaml
(Get-Content authorization-service.yaml).replace('authz-service:latest', 'authz-service:' + $Env:RELEASE_ARTIFACTS_AUTHZ_SERVICE_BUILDID) | Set-Content authorization-service.yaml
(Get-Content token-service.yaml).replace('token-service:latest', 'token-service:' + $Env:RELEASE_ARTIFACTS_TOKEN_SERVICE_BUILDID) | Set-Content token-service.yaml
(Get-Content editions-service.yaml).replace('editions-service:latest', 'editions-service:' + $Env:RELEASE_ARTIFACTS_EDITIONS_SERVICE_BUILDID) | Set-Content editions-service.yaml
(Get-Content authors-service.yaml).replace('authors-service:latest', 'authors-service:' + $Env:RELEASE_ARTIFACTS_AUTHORS_SERVICE_BUILDID) | Set-Content authors-service.yaml
(Get-Content sessions-service.yaml).replace('sessions-service:latest', 'sessions-service:' + $Env:RELEASE_ARTIFACTS_SESSIONS_SERVICE_BUILDID) | Set-Content sessions-service.yaml



(Get-Content authentication-service.yaml)