import { Controller, Post, Headers, Res } from '@nestjs/common';



@Controller('/api/authz/')
export class AuthorizationController {

    @Post('api/register/')
    async authorizeRegistrationRequests(
                @Headers('x-application-key') applicationKey: string,
                @Res() response
    ) {     
        console.log(applicationKey);
        if (applicationKey === 'let-me-pass') {
            response.sendStatus(200);
        }
        else {
            response.sendStatus(401);
        }

        
        response.end();
    }
}