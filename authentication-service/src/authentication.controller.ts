import { Controller, Post, Body, Response, HttpStatus } from '@nestjs/common';
import { TokenClient } from './token.client';
import { AuthenticationService } from './authentication.service';

@Controller('api/auth')
export class AuthenticationController {

    constructor(private authenticationService: AuthenticationService,
                private tokenClient: TokenClient) {

    }
    @Post()
    async login( @Body() request, @Response() response) {
        
        if (!request.username || !request.password) {
            response.status(HttpStatus.FORBIDDEN)
                    .json({message: 'Username and password required'});
            return;
        }

        const userInfo = await this.authenticationService.getUser(request.username);        
        if (!userInfo) {            
            response.status(HttpStatus.FORBIDDEN)
                    .json({message: 'Invalid user or bad password'});
            return;
        }
        
        const hashedPassword = this.authenticationService
            .getHashedPassword(request.password, userInfo.salt);
        
        if (hashedPassword !== userInfo.hashedPassword) {
            response.status(HttpStatus.FORBIDDEN)
                    .json({message: 'Invalid user or bad password'});
            return;
        }

        if (!userInfo.active) {            
            response.status(HttpStatus.FORBIDDEN)
                    .json({message: 'User login has been disabled! Please contact the administrator'});
            return;
        } 

        let user = { username: request.username };            
        let token = await this.tokenClient.getToken(user);            
        response.status(HttpStatus.OK)
                .json(token);
    
    }

    
}