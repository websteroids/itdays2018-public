export class UserLoginModel {
    username: string;

    hashedPassword: string;

    salt: string;  
    
    active: boolean;
}