import { Injectable } from "@nestjs/common";
import * as request from 'request';

@Injectable()
export class TokenClient {

    async getToken(user: {}): Promise<{}> {        
        return new Promise<{}> ( (resolve, reject) => {
            request.post(
                {
                    headers: {'content-type' : 'application/javascript'},
                    url:     'http://token-service:3002/api/token-encoding-requests/',
                    body:    JSON.stringify(user)
                }, (error, response, body) => {
                    if (error) {
                        reject(error);
                    } else {                        
                        resolve(JSON.parse(body));
                    }                    
                });
        })
            
    }

}
