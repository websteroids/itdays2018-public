import { Module } from '@nestjs/common';

import { AuthenticationController } from './authentication.controller';
import { AuthenticationService } from './authentication.service';
import { TokenClient } from './token.client';

@Module({
  controllers: [AuthenticationController],
  providers: [ AuthenticationService, TokenClient]

})
export class AppModule {}