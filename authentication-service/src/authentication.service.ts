import { Injectable } from '@nestjs/common';
import * as crypto from 'crypto';
import { UserLoginModel } from './userlogin.model';

const users = [
    {username: "user@domain.com", password: "password", salt: "randomSalt", active: true},
    {username: "user2@domain.com", password: "password2", salt: "randomSalt2", active: false}
]

@Injectable()
export class AuthenticationService {

    constructor() { }

    async getUser(username: string): Promise<UserLoginModel> {
        //return this.dbService.auditors.findOne({email:email});
        
        return new Promise<UserLoginModel>((resolve, reject) => {
            const user = users.find( (item) => item.username == username);                    
            if (user) {
                resolve({
                    salt: user.salt,
                    hashedPassword: this.getHashedPassword(user.password, user.salt),
                    username: user.username,
                    active: user.active                    
                });                
            } else {
                resolve(null);
            }
        });
    }

    getHashedPassword(password: string, salt: string) {
        return crypto.createHash('sha256')
                            .update(password + salt)
                            .digest('base64');
    }
}