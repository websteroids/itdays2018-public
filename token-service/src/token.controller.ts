import { Controller, Post, Body } from "@nestjs/common";
import { TokenService } from "./token.service";

@Controller('api')
export class TokenController {

    constructor(private tokenService: TokenService) {
        
    }

    @Post('/token-encoding-requests/')
    createToken(@Body() tokenContent: {}) {        
        
        const token = this.tokenService.createToken(tokenContent);

        return  {
            token: token
        }
    }

    @Post('/token-decoding-requests/')
    async decodeToken(@Body() tokenContent: {token: string}) {                
        return await this.tokenService.decodeToken(tokenContent.token);               
    }

    @Post('/token-refreshing-requests/')
    async refreshToken(@Body() tokenContent: {token: string}) {                
        const decodedToken = await this.tokenService.decodeToken(tokenContent.token);               
        
        const newToken = this.tokenService.createToken(decodedToken);

        return  {
            token: newToken,
            data: decodedToken
        }      
    }
}