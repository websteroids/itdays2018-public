import { Injectable } from "@nestjs/common";
import * as fs from 'fs';
import * as jwt from 'jsonwebtoken';

@Injectable()
export class TokenService {
    decodeToken(token: string ): Promise<{}> {        
        const cert = fs.readFileSync('keys/auth-converted.pub');

        return new Promise<{}>((resolve, reject) => {
            jwt.verify(token, cert, { algorithms: ['RS256'] }, (error, decoded) => {    
                if (error) {
                    reject(error);
                }
                delete decoded['iat'];
                delete decoded['exp'];        
                resolve(decoded);
            });
        });

    }
    
    createToken(tokenContent: {}): any {        
        let cert = fs.readFileSync('keys/auth');        
        let token = jwt.sign(tokenContent, cert, {
            algorithm: 'RS256',
            expiresIn: 1440000 
        });

        return token;
    }
}